import { Component, OnInit } from "@angular/core";
import { UploadService } from "../upload.service";
@Component({
  selector: "app-history",
  templateUrl: "./history.component.html",
  styleUrls: ["./history.component.scss"],
})
export class HistoryComponent implements OnInit {
  public files: any[] = [];
  constructor(private uploadService: UploadService) {
    this.files = this.uploadService.getHistory();
  }

  ngOnInit(): void {}

  onClear() {
    this.uploadService.clearHistory();
  }
}
