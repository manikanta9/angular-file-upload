import { HttpClient, HttpEventType } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, Subject, throwError } from "rxjs";
import { catchError, map } from "rxjs/operators";

@Injectable({
  providedIn: "root",
})
export class UploadService {
  private subject = new Subject<any>();
  progress: number;
  files: string[] = [];

  constructor(private httpClient: HttpClient) {}

  public upload(formData: any): Promise<any> {
    console.log(formData);
    this.files.push(formData.data.name);
    return this.httpClient
      .post("yout-url-here", formData, {
        reportProgress: true,
        observe: "events",
      })
      .pipe(
        map((event: any) => {
          if (event.type == HttpEventType.UploadProgress) {
            this.progress = Math.round((100 / event.total) * event.loaded);
            this.setProgress(this.progress);
          } else if (event.type == HttpEventType.Response) {
            this.progress = null;
            this.setProgress(null);
          }
        }),
        catchError((err: any) => {
          this.progress = null;
          // alert(err.message);
          return throwError(err.message);
        })
      )
      .toPromise();
  }

  setProgress(progress: any) {
    this.subject.next({ progress });
  }

  clearMessages() {
    this.subject.next();
  }

  getProgress(): Observable<any> {
    return this.subject.asObservable();
  }

  public clearHistory(): void {
    this.files = [];
  }

  public getHistory(): string[] {
    return this.files;
  }
}
