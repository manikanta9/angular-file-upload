import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { marker } from "@biesbjerg/ngx-translate-extract-marker";
import { FilesComponent } from "./files.component";
const routes: Routes = [
  // Module is lazy loaded, see app-routing.module.ts
  { path: "", component: FilesComponent, data: { title: marker("files") } },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [],
})
export class FilesRoutingModule {}
