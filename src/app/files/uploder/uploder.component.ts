import { HttpClient } from "@angular/common/http";
import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { ToastrService } from "ngx-toastr";
import { UploadService } from "../upload.service";

@Component({
  selector: "app-uploder",
  templateUrl: "./uploder.component.html",
  styleUrls: ["./uploder.component.scss"],
})
export class UploderComponent implements OnInit {
  @ViewChild("fileInput", { static: false }) fileInput: ElementRef;
  public files: any[] = [];
  public filename = "";
  progress: number;
  showProgress: any = false;
  editable: any = false;

  constructor(private uploadService: UploadService, private toastr: ToastrService) {
    this.uploadService.getProgress().subscribe((data) => {
      this.showProgress = false;
      if (data) {
        this.showProgress = true;
        this.progress = data.progress;
      }
    });
  }

  ngOnInit(): void {}

  onBrowse() {
    this.files = [];
    const fileInput = this.fileInput.nativeElement;
    fileInput.onchange = () => {
      for (let index = 0; index < fileInput.files.length; index++) {
        console.log(fileInput.files[index]);
        const file = fileInput.files[index];
        this.filename = fileInput.files[index].name;
        this.files.push({ data: file, inProgress: false, progress: 0 });
      }
    };
    fileInput.click();
  }

  callUploadService(file: any) {
    const formData = new FormData();
    formData.append("file", file.data);
    file.inProgress = true;
    this.uploadService
      .upload(file)
      .then((_) => {
        this.toastr.success("File Upload!", "File Upload! Success!");
      })
      .catch((error) => {
        this.toastr.success("File Upload!", "File Upload! Success!");
      });
  }

  public onUpload(): void {
    this.fileInput.nativeElement.value = "";
    this.files.forEach((file) => {
      this.callUploadService(file);
    });
  }

  public onClear(): void {
    this.filename = "";
    this.showProgress = false;
    this.fileInput.nativeElement.value = "";
  }
}
